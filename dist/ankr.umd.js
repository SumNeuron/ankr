(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (factory((global.ankr = {})));
}(this, (function (exports) { 'use strict';

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
  }

  function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
  }

  function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

      return arr2;
    }
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _iterableToArray(iter) {
    if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
  }

  function _iterableToArrayLimit(arr, i) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance");
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance");
  }

  function pageWidth() {
    // comes from jQuery, a reliable way to get the width of the page
    return Math.max(document.body.scrollWidth, document.documentElement.scrollWidth, document.body.offsetWidth, document.documentElement.offsetWidth, document.documentElement.clientWidth);
  }
  function pageHeight() {
    // comes from jQuery, a reliable way to get the height of the page
    return Math.max(document.body.scrollHeight, document.documentElement.scrollHeight, document.body.offsetHeight, document.documentElement.offsetHeight, document.documentElement.clientHeight);
  }

  var HORIZONTAL_NUMBER_OF_ANCHORS = 3; // how many anchor points you want vertically

  var VERTICAL_NUMBER_OF_ANCHORS = 3; // anchors just around the edge, or in middle of the screen as well

  var USE_EDGE_ONLY = true;
  function anchor(_ref) {
    var dimension = _ref.dimension,
        _ref$space = _ref.space,
        space = _ref$space === void 0 ? 0 : _ref$space,
        _ref$padding = _ref.padding,
        padding = _ref$padding === void 0 ? 0 : _ref$padding,
        _ref$margin = _ref.margin,
        margin = _ref$margin === void 0 ? 0 : _ref$margin,
        _ref$nth = _ref.nth,
        nth = _ref$nth === void 0 ? 0 : _ref$nth,
        _ref$anchors = _ref.anchors,
        anchors = _ref$anchors === void 0 ? 3 : _ref$anchors;

    /*
    calculates the nth anchor position (out of anchors) taking into account the space of the
    element, padding and a marging
    */
    var chunk = (dimension - space - padding + margin) / (anchors - 1);
    return nth * chunk + (padding + margin) / 2;
  }
  function horizontalAnchors(_ref2) {
    var _ref2$dimension = _ref2.dimension,
        dimension = _ref2$dimension === void 0 ? pageWidth() : _ref2$dimension,
        _ref2$space = _ref2.space,
        space = _ref2$space === void 0 ? 0 : _ref2$space,
        _ref2$padding = _ref2.padding,
        padding = _ref2$padding === void 0 ? 0 : _ref2$padding,
        _ref2$margin = _ref2.margin,
        margin = _ref2$margin === void 0 ? 0 : _ref2$margin,
        _ref2$anchors = _ref2.anchors,
        anchors = _ref2$anchors === void 0 ? HORIZONTAL_NUMBER_OF_ANCHORS : _ref2$anchors;
    // gets the x anchor positions
    return _toConsumableArray(Array(anchors)).map(function (e, i) {
      return anchor({
        dimension: dimension,
        space: space,
        padding: padding,
        margin: margin,
        nth: i,
        anchors: anchors
      });
    });
  }
  function verticalAnchors(_ref3) {
    var _ref3$dimension = _ref3.dimension,
        dimension = _ref3$dimension === void 0 ? pageHeight() : _ref3$dimension,
        _ref3$space = _ref3.space,
        space = _ref3$space === void 0 ? 0 : _ref3$space,
        _ref3$padding = _ref3.padding,
        padding = _ref3$padding === void 0 ? 0 : _ref3$padding,
        _ref3$margin = _ref3.margin,
        margin = _ref3$margin === void 0 ? 0 : _ref3$margin,
        _ref3$anchors = _ref3.anchors,
        anchors = _ref3$anchors === void 0 ? VERTICAL_NUMBER_OF_ANCHORS : _ref3$anchors;
    // gets the y anchor positions
    return _toConsumableArray(Array(anchors)).map(function (e, i) {
      return anchor({
        dimension: dimension,
        space: space,
        padding: padding,
        margin: margin,
        nth: i,
        anchors: anchors
      });
    });
  }
  function isEdgeAnchor(_ref4, horizontalAnchors, verticalAnchors) {
    var _ref5 = _slicedToArray(_ref4, 2),
        x = _ref5[0],
        y = _ref5[1];

    var nH = horizontalAnchors.length;
    var nV = verticalAnchors.length;
    return x === horizontalAnchors[0] || x === horizontalAnchors[nH - 1] || y === verticalAnchors[0] || y === verticalAnchors[nV - 1];
  }
  function anchors(_ref6) {
    var _ref6$dimension = _ref6.dimension,
        dimension = _ref6$dimension === void 0 ? [pageWidth(), pageHeight()] : _ref6$dimension,
        _ref6$space = _ref6.space,
        space = _ref6$space === void 0 ? [0, 0] : _ref6$space,
        _ref6$padding = _ref6.padding,
        padding = _ref6$padding === void 0 ? [0, 0] : _ref6$padding,
        _ref6$margin = _ref6.margin,
        margin = _ref6$margin === void 0 ? [0, 0] : _ref6$margin,
        _ref6$anchors = _ref6.anchors,
        anchors = _ref6$anchors === void 0 ? [HORIZONTAL_NUMBER_OF_ANCHORS, VERTICAL_NUMBER_OF_ANCHORS] : _ref6$anchors,
        _ref6$edgeOnly = _ref6.edgeOnly,
        edgeOnly = _ref6$edgeOnly === void 0 ? USE_EDGE_ONLY : _ref6$edgeOnly;
    // gets the [x, y] anchor position pairs
    var ha = horizontalAnchors({
      dimension: dimension[0],
      space: space[0],
      padding: padding[0],
      margin: margin[0],
      anchors: anchors[0]
    });
    var va = verticalAnchors({
      dimension: dimension[1],
      space: space[1],
      padding: padding[1],
      margin: margin[1],
      anchors: anchors[1]
    });
    return ha.map(function (h, i) {
      return va.map(function (v, j) {
        return [h, v];
      });
    }).flat().filter(function (_ref7) {
      var _ref8 = _slicedToArray(_ref7, 2),
          x = _ref8[0],
          y = _ref8[1];

      return edgeOnly ? isEdgeAnchor([x, y], ha, va) : true;
    });
  }
  function closestAnchor(_ref9, anchors) {
    var _ref10 = _slicedToArray(_ref9, 2),
        x = _ref10[0],
        y = _ref10[1];

    // gets the closest anchor
    var euclid = anchors.map(function (_ref11) {
      var _ref12 = _slicedToArray(_ref11, 2),
          x2 = _ref12[0],
          y2 = _ref12[1];

      return Math.sqrt(Math.pow(x - x2, 2) + Math.pow(y - y2, 2));
    });
    var min = Math.min.apply(Math, _toConsumableArray(euclid));
    var pos = euclid.indexOf(min);
    pos = pos > -1 ? pos : 0;
    var closest = anchors[pos];
    return [pos, closest];
  }

  var ankr = {
    HORIZONTAL_NUMBER_OF_ANCHORS: HORIZONTAL_NUMBER_OF_ANCHORS,
    VERTICAL_NUMBER_OF_ANCHORS: VERTICAL_NUMBER_OF_ANCHORS,
    USE_EDGE_ONLY: USE_EDGE_ONLY,
    anchor: anchor,
    anchors: anchors,
    closestAnchor: closestAnchor,
    horizontalAnchors: horizontalAnchors,
    verticalAnchors: verticalAnchors,
    isEdgeAnchor: isEdgeAnchor,
    pageWidth: pageWidth,
    pageHeight: pageHeight
  };

  exports.default = ankr;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
