# ankr

Welcome to ankr. Ankr is a lightweight JavaScript library to aid in calculating
the position of "anchor" points on the webpage and finding the closest anchor
points taking into account element size, padding and margins. This makes it
easy to develop draggable elements, which can then "snap" into place.


# API
Of the functions included in ankr, `anchors` and `closestAnchor` are the two functions which one will spend the most time with:

## `anchors`

```
anchors({
 dimension = [pageWidth(), pageHeight()],
 space = [0, 0],
 padding = [0, 0],
 margin = [0, 0],
 anchors = [HORIZONTAL_NUMBER_OF_ANCHORS, VERTICAL_NUMBER_OF_ANCHORS],
 edgeOnly = USE_EDGE_ONLY
})
```

The parameters `dimension`, `space`, `padding`, `margin`, and `anchors` each _expect_ a list of __2__ elements. The first element of the list corresponds to the x value and the second the y value. The parameter `edgeOnly` is a boolean.

### Parameters
- `dimension`: the dimension of the webpage given as `[width, height]`. If not specified, will calculated on function call. So make sure to recall on window resize.
- `space`: the space the element which is to be aligned to the anchors takes up given as `[width, height]`.
- `padding`: the padding to give the element at each anchor, given as `[padX, padY]`.
- `margin`: the margin to give the element at each anchor, given as `[marX, marY]`.
- `anchors`: the number of anchors to make in each dimension, given as `[nHorizontalAnchors, nVerticalAnchors]`.
- `edgeOnly`: a boolean specifying whether if anchors are only to be made around the border of the screen (at the "edge") or if they should also fill the screen.

### Returns
returns a list of two-element arrays, each of which corresponds to the `[x, y]` position of an anchor.



## `closestAnchor`
```
closestAnchor([x, y], anchors)
```

### Parameters
-`[x, y]`: the current `[x, y]` position of the element
- `anchors`: a list of two-element arrays, each of which corresponds to the `[x, y]` position of an anchor.

### Returns
returns a two-element array, `[x, y]`, which specifies the location of the closet anchor, taking into account the padding, margin, and size of the element when those anchors were calculated
