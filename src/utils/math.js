import {pageWidth, pageHeight} from './window.js';


// how many anchor points you want horizontally
let HORIZONTAL_NUMBER_OF_ANCHORS = 3;
// how many anchor points you want vertically
let VERTICAL_NUMBER_OF_ANCHORS = 3;
// anchors just around the edge, or in middle of the screen as well
let USE_EDGE_ONLY = true;


export function anchor({
  dimension, // the dimension's length to divid
  space = 0, // the space (along the provided dimension) of the object to anchor
  padding = 0, // how much to pad (symmetrically) the object
  margin = 0, // how much margin to add (symmetrically) to the object
  nth = 0, // which anchor to calculate (0-indexed)
  anchors = 3 // total number of anchors along
}) {
  /*
  calculates the nth anchor position (out of anchors) taking into account the space of the
  element, padding and a marging
  */
  let chunk = (dimension - space - padding + margin) / (anchors - 1);
  return nth * chunk + (padding + margin) / 2;
}

export function horizontalAnchors({
  dimension = pageWidth(),
  space = 0,
  padding = 0,
  margin = 0,
  anchors = HORIZONTAL_NUMBER_OF_ANCHORS
}) {
  // gets the x anchor positions
  return [...Array(anchors)].map((e, i) =>
    anchor({
      dimension,
      space,
      padding,
      margin,
      nth: i,
      anchors
    })
  );
}

export function verticalAnchors({
  dimension = pageHeight(),
  space = 0,
  padding = 0,
  margin = 0,
  anchors = VERTICAL_NUMBER_OF_ANCHORS
}) {
  // gets the y anchor positions
  return [...Array(anchors)].map((e, i) =>
    anchor({
      dimension,
      space,
      padding,
      margin,
      nth: i,
      anchors
    })
  );
}

export function isEdgeAnchor([x, y], horizontalAnchors, verticalAnchors) {
  let nH = horizontalAnchors.length;
  let nV = verticalAnchors.length;
  return (
    (x === horizontalAnchors[0] || x === horizontalAnchors[nH-1]) ||
    (y === verticalAnchors[0]   || y === verticalAnchors[nV-1])
  )
}

export function anchors({
  dimension = [pageWidth(), pageHeight()],
  space = [0, 0],
  padding = [0, 0],
  margin = [0, 0],
  anchors = [HORIZONTAL_NUMBER_OF_ANCHORS, VERTICAL_NUMBER_OF_ANCHORS],
  edgeOnly = USE_EDGE_ONLY
}) {
  // gets the [x, y] anchor position pairs
  let ha = horizontalAnchors({
    dimension: dimension[0],
    space: space[0],
    padding: padding[0],
    margin: margin[0],
    anchors: anchors[0]
  });
  let va = verticalAnchors({
    dimension: dimension[1],
    space: space[1],
    padding: padding[1],
    margin: margin[1],
    anchors: anchors[1]
  });

  return ha.map((h, i) => va.map((v, j) => [h, v])).flat().filter(
    ([x, y]) => edgeOnly ? isEdgeAnchor([x, y], ha, va) : true
  );
}

export function closestAnchor([x, y], anchors) {
  // gets the closest anchor
  let euclid = anchors.map(([x2, y2]) =>
    Math.sqrt(Math.pow(x - x2, 2) + Math.pow(y - y2, 2))
  );
  let min = Math.min(...euclid);
  let pos = euclid.indexOf(min);

  pos = pos > -1 ? pos : 0;
  let closest = anchors[pos];
  return [pos, closest];
}


export { HORIZONTAL_NUMBER_OF_ANCHORS, VERTICAL_NUMBER_OF_ANCHORS, USE_EDGE_ONLY };
