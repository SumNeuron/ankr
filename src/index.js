import {
  HORIZONTAL_NUMBER_OF_ANCHORS, VERTICAL_NUMBER_OF_ANCHORS,
  USE_EDGE_ONLY,
  anchor, anchors, closestAnchor,
  horizontalAnchors, verticalAnchors, isEdgeAnchor
} from './utils/math.js'
import {pageWidth, pageHeight} from './utils/window.js'


let ankr = {
  HORIZONTAL_NUMBER_OF_ANCHORS, VERTICAL_NUMBER_OF_ANCHORS,
  USE_EDGE_ONLY,
  anchor, anchors, closestAnchor,
  horizontalAnchors, verticalAnchors, isEdgeAnchor,
  pageWidth, pageHeight
}

export default ankr
